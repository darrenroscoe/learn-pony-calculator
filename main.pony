use "files"

actor Main
  let env: Env

  new create(env': Env) =>
    env = env'
    let examples = [
      "(+ 1 2)"
      "(- 9 6)"
      "(+ (+ 4 5) (- 8 2))"
      "(+ 2 (+ 4 (- (+ 9 (+ 4 (+ 2 (+ 7 (- 20 3))))) 4)))"
      "(+ 234224837 (+ 271252186 88752224))"
    ]
    env.out.print("Let's get calculating!\n")
    if env.args.size() == 2 then
      from_file()
      return
    end
    let app = App(env)
    for input in examples.values() do
      app.run(input)
    end


  fun from_file() =>
    let app = App(env)
    try
      let arg = env.args(1)
      let input = get_input_from_file(arg)
      app.run(input)
    else
      env.out.print("App failed, begin panicking")
    end


  fun get_input_from_file(file_name: String): String =>
    try
      let path = FilePath(env.root as AmbientAuth, file_name)
      match OpenFile(path)
      | let file: File =>
        recover val file.read_string(1028) end
      else
        env.out.print("Opening the file didn't work, try again")
        recover val String end
      end
    else
      recover val String end
    end


actor App
  let env: Env

  new create(env': Env) =>
    env = env'

  be run(expr: String) =>
    env.out.print(expr)
    Tokenizer.tokenize(expr, this)

  be tokenized(tokens: Array[Token] val) =>
    Parser.parse(tokens, this)

  be parsed(parsed_expr: Expr val) =>
    env.out.print(parsed_expr.value().string())

  be failed(reason: String) =>
    env.out.print(reason)

  be report(message: String) =>
    env.out.print("\n==========")
    env.out.print(message)
    env.out.print("==========\n")
