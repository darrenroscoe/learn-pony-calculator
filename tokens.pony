primitive Open
  fun tag string(): String iso^ =>
    let s: String iso = recover String end
    s.append("Open")
    s

primitive Close
  fun tag string(): String iso^ =>
    let s: String iso = recover String end
    s.append("Close")
    s

class val Num
  let _val: U64

  new val create(a: U64) =>
    _val = a

  fun value(): U64 => _val

  fun string(): String iso^ => _val.string()

trait val Op is Stringable
  fun apply(a: U64, b: U64): U64

class AddOp is Op
  fun apply(a: U64, b: U64): U64 =>
    a + b

  fun string(): String iso^ =>
    let s: String iso = recover String end
    s.append("Add")
    s

class MinusOp is Op
  fun apply(a: U64, b: U64): U64 =>
    a - b

  fun string(): String iso^ =>
    let s: String iso = recover String end
    s.append("Subtract")
    s

type Token is (Open | Close | Num | Op)

actor Tokenizer

  be tokenize(str: String, app: App) =>
    app.report("Tokenizing")
    var tokens = recover trn Array[Token](str.size()) end
    var is_num = false
    var num_so_far = recover iso String end
    for char in str.values() do
      match char
      | '(' =>
        if is_num then
          app.failed("Found a ( while reading a number")
          return
        end
        tokens.push(Open)
      | ')' =>
        if is_num then
          try tokens.push(Num(num_so_far.u64()))
          end
          is_num = false
          num_so_far = recover iso String end
        end
        tokens.push(Close)
      | '+' =>
        if is_num then
          app.failed("Found a + while reading a number")
          return
        end
        tokens.push(AddOp)
      | '-' =>
        if is_num then
          app.failed("Found a - while reading a number")
          return
        end
        tokens.push(MinusOp)
      | ' ' =>
        if is_num then
          try tokens.push(Num(num_so_far.u64()))
          end
          is_num = false
          num_so_far = recover iso String end
        end
      | let n: U8 =>
        is_num = true
        num_so_far.push(n)
      end
    end
    app.tokenized(consume tokens)
