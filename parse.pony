actor Parser

  be parse(tokens: Array[Token] val, app: App) =>
    app.report("Parsing")
    let parsed_expression = parse_expr(tokens.values())
    try
      let noex = parsed_expression as NoExpr val
      app.failed(noex.reason())
      return
    end
    app.parsed(parsed_expression)

  fun parse_expr(tokens: Iterator[Token]): Expr val =>
    try
      while tokens.has_next() do
        let next = tokens.next()
        match next
        | let n: Num => return NumExpr(n.value())
        | let o: Open =>
          let op = try tokens.next() as Op else return NoExpr("Open was not followed by an Op") end
          let l = parse_expr(tokens)
          try return l as NoExpr val end
          let r = parse_expr(tokens)
          try return r as NoExpr val end
          try
            match tokens.next()
            | Close => return OpExpr(op, l, r)
            else
              return NoExpr("Did not find a mathing close paren")
            end
          else
            return NoExpr("Reached end of tokens before finishing an expression")
          end
        else
          return NoExpr("Found invalid beginning of expression, must be Open or Num")
        end
      else
        NoExpr("Reached the end of the tokens without finding a complete expression")
      end
    else
      NoExpr("Oh bother something went terribly, terribly wrong")
    end

trait Expr
  fun value(): U64

class NoExpr is Expr

  let _reason: String

  new val create(reason': String) =>
    _reason = reason'

  fun value(): U64 => 0

  fun val reason(): String => _reason

class NumExpr is Expr
  let _num: U64

  new val create(num: U64) =>
    _num = num

  fun value(): U64 => _num

class OpExpr is Expr
  let _op: Op
  let _l: Expr val
  let _r: Expr val

  new val create(op: Op, l: Expr val, r: Expr val) =>
    _op = op
    _l = l
    _r = r

  fun value(): U64 => _op(_l.value(), _r.value())
